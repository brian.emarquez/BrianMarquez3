<h1 align="center">Hi 👋, I'm Brian Marquez</h1> 

<p align="left"> 
 <img src="https://img.shields.io/github/followers/BrianMarquez3?label=Follow&style=social"> 
  
<img src="https://komarev.com/ghpvc/?username=brianmarquez3&label=Profile%20views&color=0e75b6&style=flat" alt="brianmarquez3"/>
  
<a href="https://www.linkedin.com/in/brian-enrique-marquez-inca-roca-57a28a1b1/">
<img  alt="LinkedIn" src="https://img.shields.io/badge/-Brian%20Marquez-blue"/>

<a href="mailto:brian3marquez@gmail.com">
<img a alt="E-mail" src="https://img.shields.io/badge/-Contact%20%20%20me-red" />

</p> 

<table>
  <tr>
    <td align="center" style="padding=0;width=50%;">
      <img align="center" style="padding=0;" src="https://github-readme-stats-eight-theta.vercel.app/api?username=brianmarquez3&show_icons=true&include_all_commits=true&count_private=true&bg_color=1c1c1c&hide_border=true&text_color=ffffff&title_color=c3002f&icon_color=c3002f&hide_title=true" />
    </td>
    <td align="center" style="padding=0;width=50%;">
      <img align="center" style="padding=0;" src="https://github-readme-stats.quantumlytangled.vercel.app/api/top-langs/?username=brianmarquez3&layout=compact&bg_color=1c1c1c&hide_border=true&text_color=ffffff&title_color=c3002f&icon_color=c3002f&hide_title=true&count_private=true&extra=rysemultiplayer/template,bot,bot-kernel,bot-pieces,bot-plugins,logger,vscode-autocomplete" />
    </td>
  </tr>
</table>

### Tecnologias

![Python](https://img.shields.io/badge/-Python-black?style=flat-square&logo=Python)
![C++](https://img.shields.io/badge/-C++-black?style=flat-square&logo=c)
![Java](https://img.shields.io/badge/-java-black?style=flat-square&logo=java)
![PHP](https://img.shields.io/badge/-PHP-black?style=flat-square&logo=php)
![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript)
![Nodejs](https://img.shields.io/badge/-Nodejs-black?style=flat-square&logo=Node.js)
![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-black?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-black?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-black?style=flat-square&logo=bootstrap)
![MongoDB](https://img.shields.io/badge/-MongoDB-black?style=flat-square&logo=mongodb)
![GraphQL](https://img.shields.io/badge/-GraphQL-black?style=flat-square&logo=graphql)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-black?style=flat-square&logo=postgresql)
![MySQL](https://img.shields.io/badge/-MySQL-black?style=flat-square&logo=mysql)
![Heroku](https://img.shields.io/badge/-Heroku-black?style=flat-square&logo=heroku)
![Docker](https://img.shields.io/badge/-Docker-black?style=flat-square&logo=docker)
![Firebase](https://img.shields.io/badge/Firebase-black?style=flat-square&logo=firebase)
![Git](https://img.shields.io/badge/-Git-black?style=flat-square&logo=git)
![GitHub](https://img.shields.io/badge/-GitHub-black?style=flat-square&logo=github)
![Arduino](https://img.shields.io/badge/-Arduino-black?style=flat-square&logo=arduino)
![R](https://img.shields.io/badge/-R-black?style=flat-square&logo=r)


<hr/>
<div align="center">
<img src="https://github-profile-trophy.vercel.app/?username=BrianMarquez3&theme=flat&no-frame=true&margin-w=30&no-bg=true" />


<hr/>
